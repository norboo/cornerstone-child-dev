<?php
/**
 * The template used for displaying page content.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 4.2.4.1
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>

	<?php do_action( 'cornerstone_page_before_entry_content' ); ?>
	<div class="entry-content">
        <?php the_excerpt();?>
		<?php the_content(); ?>
	</div>
	<?php do_action( 'cornerstone_page_after_entry_content' ); ?>

</article>
