<?php

// Enqueue CSS and scripts

function load_cornerstone_child_scripts() {
	wp_enqueue_style(
		'cornerstone_child_css',
		get_stylesheet_directory_uri() . '/style.css',
		array('foundation_css'),
		false,
		'all'
	);
}

add_action('wp_enqueue_scripts', 'load_cornerstone_child_scripts',50);

// Github Updater
define( 'GITHUB_UPDATER_OVERRIDE_DOT_ORG', true );

// Carbon Fields
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_post_meta' );
function crb_attach_post_meta() {
    Container::make( 'post_meta', __('Custom Excerpt', 'cornerstone') )
        ->where( 'post_type', 'IN', array('page','post') )
        ->set_context('carbon_fields_after_title')
        ->add_fields(array(
            Field::make( 'rich_text', 'post_excerpt','')
    ));
}


// Remove Excerpt Boxes
function remove_normal_excerpt() {
    remove_meta_box( 'postexcerpt' , 'post' , 'normal' );
}
add_action( 'admin_menu' , 'remove_normal_excerpt' );

function filter_function_name( $excerpt ) {
    $excerpt = carbon_get_the_post_meta( 'post_excerpt' );
    return $excerpt;
}
add_filter( 'get_the_excerpt', 'filter_function_name' );

function theme_prefix_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'theme_prefix_rewrite_flush' );


// Remove category base in permalinks
// Refresh rules on activation/deactivation/category changes
// Plugin URI: http://wordpresssupplies.com/wordpress-plugins/no-category-base/
register_activation_hook(__FILE__,'no_category_base_refresh_rules');
add_action('created_category','no_category_base_refresh_rules');
add_action('edited_category','no_category_base_refresh_rules');
add_action('delete_category','no_category_base_refresh_rules');
function no_category_base_refresh_rules() {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
}
register_deactivation_hook(__FILE__,'no_category_base_deactivate');
function no_category_base_deactivate() {
        remove_filter('category_rewrite_rules', 'no_category_base_rewrite_rules'); // We don't want to insert our custom rules again
        no_category_base_refresh_rules();
}
 
// Remove category base
add_filter('category_link', 'no_category_base',1000,2);
function no_category_base($catlink, $category_id) {
        $category = &get_category( $category_id );
        if ( is_wp_error( $category ) )
                return $category;
        $category_nicename = $category->slug;
       
        if ( $category->parent == $category_id ) // recursive recursion
                $category->parent = 0;
        elseif ($category->parent != 0 )
                $category_nicename = get_category_parents( $category->parent, false, '/', true ) . $category_nicename;
       
        $catlink = trailingslashit(get_option( 'home' )) . user_trailingslashit( $category_nicename, 'category' );
        return $catlink;
}
 
// Add our custom category rewrite rules
add_filter('category_rewrite_rules', 'no_category_base_rewrite_rules');
function no_category_base_rewrite_rules($category_rewrite) {
        //print_r($category_rewrite); // For Debugging
       
        $category_rewrite=array();
        $categories=get_categories(array('hide_empty'=>false));
        foreach($categories as $category) {
                $category_nicename = $category->slug;
                if ( $category->parent == $category->cat_ID ) // recursive recursion
                        $category->parent = 0;
                elseif ($category->parent != 0 )
                        $category_nicename = get_category_parents( $category->parent, false, '/', true ) . $category_nicename;
                $category_rewrite['('.$category_nicename.')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
                $category_rewrite['('.$category_nicename.')/page/?([0-9]{1,})/?$'] = 'index.php?category_name=$matches[1]&paged=$matches[2]';
                $category_rewrite['('.$category_nicename.')/?$'] = 'index.php?category_name=$matches[1]';
        }
        // Redirect support from Old Category Base
        global $wp_rewrite;
        $old_base = $wp_rewrite->get_category_permastruct();
        $old_base = str_replace( '%category%', '(.+)', $old_base );
        $old_base = trim($old_base, '/');
        $category_rewrite[$old_base.'$'] = 'index.php?category_redirect=$matches[1]';
       
        //print_r($category_rewrite); // For Debugging
        return $category_rewrite;
}
 
// Add 'category_redirect' query variable
add_filter('query_vars', 'no_category_base_query_vars');
function no_category_base_query_vars($public_query_vars) {
        $public_query_vars[] = 'category_redirect';
        return $public_query_vars;
}
// Redirect if 'category_redirect' is set
add_filter('request', 'no_category_base_request');
function no_category_base_request($query_vars) {
        //print_r($query_vars); // For Debugging
        if(isset($query_vars['category_redirect'])) {
                $catlink = trailingslashit(get_option( 'home' )) . user_trailingslashit( $query_vars['category_redirect'], 'category' );
                status_header(301);
                header("Location: $catlink");
                exit();
        }
        return $query_vars;
}

