<?php
/*
 * Template Name: Homepage Template
 * Description: Custom template for Homepage
 */

get_header(); ?>

<div class="section">
	<div class="grid-x grid-padding-x">
		<div class="cell section-content align-self-middle text-center">
            <h2>Intro</h2>
		</div>

	</div>
</div>

<div class="section section-2">
	<div class="grid-x grid-padding-x">
		<div class="cell section-content align-self-middle text-center">
            <h2>About Us</h2>
		</div>

	</div>
</div>

<div class="section">
	<div class="grid-x grid-padding-x">
		<div class="cell section-content align-self-middle text-center">
            <h2>Services</h2>
		</div>

	</div>
</div>

<div class="section section-2">
	<div class="grid-x grid-padding-x">
		<div class="cell section-content align-self-middle text-center">
            <h2>Testimonials</h2>
		</div>

	</div>
</div>

<?php get_footer(); ?>